window.addEventListener('load', function() {
	//stran nalozena
	
	/////pocaka da klikne gumb
	document.querySelector("#prijavniGumb").addEventListener('click', function(){
		
		//prebere ime iz boxa
		var ime = document.querySelector("#uporabnisko_ime").value;
		//spremeni element dokumenta z id uporabnik na prebrano ime
		document.querySelector("#uporabnik").innerHTML = ime;
		
		//za spreminjanje stilov uporabi querySelector
		var pokri = document.querySelector(".pokrivalo");
		pokri.style.visibility = 'hidden' ;

	});
	/////
	
	var opozori = function(id) {
		if (id == "cas")
			document.querySelector("#stevilke").classList.add("hide");
		else if (id == "opomnik")
			document.querySelector("#crke").classList.add("hide");
	};
	
	var skri= function() {
		document.querySelector("#stevilke").classList.remove("hide");
		document.querySelector("#crke").classList.remove("hide");
	};
	////
	
	var dodajOpomnik = function() {
		
		var nazivOpomnika = document.querySelector("#naziv_opomnika").value;
		var cas = document.querySelector("#cas_opomnika").value;
		
		document.querySelector("#naziv_opomnika").value = "";
		document.querySelector("#cas_opomnika").value = "";
		
		var opomniki = document.querySelector("#opomniki");
		
		if(isNaN(parseInt(cas))) {
			console.log("napaka v casu");
			opozori("cas");
		}
		else if(/\d/.test(nazivOpomnika)){
			console.log("napaka naziv");
			opozori("opomnik");
		}
		else{
				skri();
				opomniki.innerHTML += " \
					<div class ='opomnik rob senca'> \
						<div class ='naziv_opomnika'>"+nazivOpomnika+"</div> \
						<div class='cas_opomnika'> Opomnik čez <span>"+cas+"</span> sekund. </div>\
				</div>";
		}
	};
	
	document.querySelector('#dodajGumb').addEventListener('click', dodajOpomnik);

	//Posodobi opomnike
	var posodobiOpomnike = function() {
		var opomniki = document.querySelectorAll(".opomnik");

		for (var i = 0; i < opomniki.length; i++) {
			var opomnik = opomniki[i];
			var casovnik = opomnik.querySelector("span");
			var cas = parseInt(casovnik.innerHTML);

			//TODO:
			// - če je čas enak 0, izpiši opozorilo "Opomnik!\n\nZadolžitev NAZIV_OPOMNIK je potekla!"
			// - sicer zmanjšaj čas za 1 in nastavi novo vrednost v časovnikudsad
			
			if(cas > 0){
				cas --;
				casovnik.innerHTML = cas;
			} else {
				casovnik.innerHTML = "0";
				alert('Opomnik!\n\nZadolžitev'+opomnik.querySelector(".naziv_opomnika").innerHTML+'je potekla!');
				document.querySelector("#opomniki").removeChild(opomnik);
			}
		}
	}
	
	//Preverja vsako Sekundo!!!! zazene posodobi opomnike!!!
	setInterval(posodobiOpomnike, 1000);

});

